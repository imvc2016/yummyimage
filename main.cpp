// Header
#include "im.h"

// プロトタイプ宣言
void printHelp( char *);
GLvoid initgfx( GLvoid );
GLvoid drawScene( GLvoid );
GLvoid reshape( GLsizei, GLsizei );
GLvoid keyboard( GLubyte, GLint, GLint );

// キーボードASCII値
#define KEY_ESC	27

// ウィンドウのサイズ
static int winWidth;	// ウィンドウの横幅
static int winHeight;	// ウィンドウの高さ

// テクスチャデータ
static ms2dImage *imgOrg = NULL;        //最初の画像データ
static ms2dImage *imgSrc = NULL;		// 元画像のデータ
static ms2dImage *imgDst = NULL;		// 結果画像のデータ

//RGBフィルターの濃度
static double Filval = 0.05;
static double Filval2 = 1.0;

void main( int argc, char *argv[] )
{
	// GLUTにmainの引数渡して初期化
    glutInit( &argc, argv );

	// GLUTスクリーンサイズの取得
    GLsizei width  = glutGet( GLUT_SCREEN_WIDTH );
    GLsizei height = glutGet( GLUT_SCREEN_HEIGHT );
	// ウィンドウの諸設定
    glutInitWindowPosition( width / 8, height / 8 );
    glutInitWindowSize( width * 6 / 8, height * 6 / 8 );
	// 色設定
	glutInitDisplayMode( GLUT_RGBA | GL_DOUBLE );
	// ウィンドウ作成(argv[0]はタイトル)
    glutCreateWindow( argv[0] );

	// 元画像読み込み
	imgSrc = ms2dImageCreateWithFile("Image.jpg");
	// 結果画像を用意
	imgDst = ms2dImageClone(imgSrc);
	//
	imgOrg = ms2dImageClone(imgSrc);


	// ヘルプメッセージ
	printHelp( argv[1] );
	// 初期化処理
	initgfx();

	// 再描画関数の定義
	glutKeyboardFunc( keyboard );
	glutReshapeFunc( reshape );
	glutDisplayFunc( drawScene );
	
	// メインループ（GLUTに委譲）
    glutMainLoop();
}


// ヘルプメッセージ
void printHelp( char *progname )
{
	fprintf(stdout,
		"\n%s - 画像処理01:簡単なフィルタ処理\n\n"

		"[r] - 赤フィルター\n"
		"[g] - 緑フィルター\n"
		"[b] - 青フィルター\n"
		"[+] - 色フィルター濃度 プラス\n"
		"[-] - 色フィルター濃度 マイナス\n"
		"[t] - 明るさ調整\n"
		"[s] - シャドウ\n"

		"[L] - 画像読み込み\n"
		"[S] - 画像保存\n"
		"[ ] - 画像の初期化\n"
		"ESC - プログラムの終了\n\n",
		progname);
}


// 初期化処理
GLvoid initgfx( GLvoid )
{
	// 背景の色
	glClearColor(0.85, 0.88, 0.9, 1.0);
}


// 画面サイズ変更
GLvoid reshape(GLsizei width, GLsizei height)
{
	// ウィンドウ枠のビューポートを設定
    glViewport( 0, 0, width, height );
	// ウィンドウのサイズの更新
	winWidth  = width;
	winHeight = height;
}


// キーボード処理
GLvoid keyboard(GLubyte key, GLint x, GLint y)
{
	switch(key)
	{
		case ' ':
		{
			//初期化
			imgSrc = ms2dImageClone(imgOrg);
			imgDst = ms2dImageClone(imgOrg);
			msglReplaceTexture(imgDst);
			glutPostRedisplay();
			break;
		}

		case '+':
		{
			// フィルター濃度を大きくする
			Filval += 0.1;
			Filval = min(1, Filval);
			fprintf(stdout, "色フィルター濃度＝%.4f\n", Filval);
			break;
		}

		case '-':
		{
			// フィルター濃度を小さくする
			Filval -= 0.1;
			Filval = max(0, Filval);
			fprintf(stdout, "色フィルター濃度＝%.4f\n", Filval);
			break;
		}

		case 'L':
		{
			// 読込用ファイルダイアログを開く
			char path[256];
			if(msglOpenDialog(path)){
				// imgSrcにファイルを読み込む
				ms2dImageLoadFile(imgSrc, path);
				// msglのテクスチャを新しいデータに差し替え
				msglReplaceTexture(imgSrc);
				imgDst = ms2dImageClone(imgSrc);
				msglReplaceTexture(imgDst);
				imgOrg = ms2dImageClone(imgSrc);
				// 再描画
				glutPostRedisplay();
			}
			break;
		}
		case 'S':
		{
			// 保存用ファイルダイアログを開く
			char path[256];
			if(msglSaveDialog(path)){
				// imgDstをファイルに保存する
				ms2dImageSaveFile(imgDst, path);
			}
			break;
		}

		case 'r':
		{
			// Red抽出
			imFilterRed(imgSrc, imgDst, Filval);
			ms2dImageCopy(imgDst, imgSrc);
			// msglのテクスチャを新しいデータに差し替え
			msglReplaceTexture(imgDst);
			// 再描画
			glutPostRedisplay();
			break;
		}

		case 'g':
		{
			// Green抽出
			imFilterGreen(imgSrc, imgDst, Filval);
			ms2dImageCopy(imgDst, imgSrc);
			// msglのテクスチャを新しいデータに差し替え
			msglReplaceTexture(imgDst);
			// 再描画
			glutPostRedisplay();
			break;
		}

		case 'b':
		{
			// Blue抽出
			imFilterBlue(imgSrc, imgDst, Filval);
			ms2dImageCopy(imgDst, imgSrc);
			// msglのテクスチャを新しいデータに差し替え
			msglReplaceTexture(imgDst);
			// 再描画
			glutPostRedisplay();
			break;
		}

		case 't':
		{
			// 階調変換(明るく)
			imFilterContrast(imgSrc, imgDst, Filval2);
			ms2dImageCopy(imgDst, imgSrc);
			// msglのテクスチャを新しいデータに差し替え
			msglReplaceTexture(imgDst);
			// 再描画
			glutPostRedisplay();
			break;
		}
		case 's':
		{
			// 階調変換(シャドウ)
			imFilterContrast2(imgSrc, imgDst, Filval2);
			ms2dImageCopy(imgDst, imgSrc);
			// msglのテクスチャを新しいデータに差し替え
			msglReplaceTexture(imgDst);
			// 再描画
			glutPostRedisplay();
			break;
		}

		// プログラム終了
		case KEY_ESC:
		{
			exit(0);
		}
	}
}


// 描画処理
GLvoid drawScene( GLvoid )
{
	glClear( GL_COLOR_BUFFER_BIT );

	// msglの行列をセットする
	msglPushDefaultMatrix(winWidth, winHeight);
	{
		// 元画像のmsglテクスチャ平面を表示
		msglTexQuad(imgSrc);

		// 結果画像のmsglテクスチャ平面を表示
		glTranslatef(imgSrc->width+10, 0, 0);
		msglTexQuad(imgDst);
	}
	// msglの行列をクリアする
	msglPopMatrix();

	// 強制更新
	glutSwapBuffers();
}
