// Header
#include <stdio.h>			// 標準出力
#include <stdlib.h>			// 標準ライブラリ
#include <math.h>			// 数学ヘッダ
#include <GL/glut.h>		// glut
#include <msgl.h>			// msgl

// 画像処理の関数

//Red抽出
void imFilterRed( ms2dImage* imgSrc, ms2dImage* imagDst, double Filval );

//Green抽出
void imFilterGreen(ms2dImage* imgSrc, ms2dImage* imagDst, double Filval);

//Blue抽出
void imFilterBlue(ms2dImage* imgSrc, ms2dImage* imagDst, double Filval);

//諧調変換（明）
void imFilterContrast(ms2dImage* imgSrc, ms2dImage* imgDst, double Filval2);

//諧調変換（影）
void imFilterContrast2(ms2dImage* imgSrc, ms2dImage* imgDst, double Filval2);


