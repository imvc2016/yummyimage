// Header
#include "im.h"


void imFilterRed(ms2dImage* imgSrc, ms2dImage* imgDst, double Filval)
{
    //Dstにコピー
	ms2dImageCopy( imgSrc, imgDst );

	//画像サイズ取得
	int wid = imgDst->width;	// 幅
	int hgt = imgDst->height;	// 高さ

	// ピクセルアクセスの準備
	ms2dColorMatrix pxSrc = imgSrc->pixels;	// 元画像のピクセルデータ
	ms2dColorMatrix pxDst = imgDst->pixels;	// 結果画像のピクセルデータ

	ms2dImage*img1;

	// 1ピクセルずつ、すべてのピクセルを処理
	for (int y = 0; y < hgt; y++)
	{
		for (int x = 0; x < wid; x++)
		{
			// 元画像pxDstの(x, y)座標の色値を得る
			int Rval = pxDst[y][x].R;
			int Gval = pxDst[y][x].G;
			int Bval = pxDst[y][x].B;

			//Redだけ有効にする
			pxDst[y][x].R = Rval;
			pxDst[y][x].G = Gval * (1.0 - Filval);
			pxDst[y][x].B = Bval * (1.0 - Filval);

		}
	}
}

void imFilterGreen(ms2dImage* imgSrc, ms2dImage* imgDst, double Filval)
{
	//Dstにコピー
	ms2dImageCopy(imgSrc, imgDst);

	//画像サイズ取得
	int wid = imgDst->width;	// 幅
	int hgt = imgDst->height;	// 高さ

	// ピクセルアクセスの準備
	ms2dColorMatrix pxSrc = imgSrc->pixels;	// 元画像のピクセルデータ
	ms2dColorMatrix pxDst = imgDst->pixels;	// 結果画像のピクセルデータ

	// 1ピクセルずつ、すべてのピクセルを処理
	for (int y = 0; y < hgt; y++)
	{
		for (int x = 0; x < wid; x++)
		{
			// 元画像pxDstの(x, y)座標の色値を得る
			int Rval = pxDst[y][x].R;
			int Gval = pxDst[y][x].G;
			int Bval = pxDst[y][x].B;

			//Greenだけ有効にする
			pxDst[y][x].R = Rval * (1.0 - Filval);
			pxDst[y][x].G = Gval;
			pxDst[y][x].B = Bval * (1.0 - Filval);
		}
	}
}

void imFilterBlue(ms2dImage* imgSrc, ms2dImage* imgDst, double Filval)
{
	//画像サイズ取得
	int wid = imgDst->width;	// 幅
	int hgt = imgDst->height;	// 高さ

	//double Filval = 0.5;

	// ピクセルアクセスの準備
	ms2dColorMatrix pxSrc = imgSrc->pixels;	// 元画像のピクセルデータ
	ms2dColorMatrix pxDst = imgDst->pixels;	// 結果画像のピクセルデータ

	// 1ピクセルずつ、すべてのピクセルを処理
	for (int y = 0; y < hgt; y++)
	{
		for (int x = 0; x < wid; x++)
		{
			// 元画像pxDstの(x, y)座標の色値を得る
			int Rval = pxDst[y][x].R;
			int Gval = pxDst[y][x].G;
			int Bval = pxDst[y][x].B;

			//Blueだけ有効にする
			pxDst[y][x].R = Rval * (1.0 - Filval); 
			pxDst[y][x].G = Gval * (1.0 - Filval);
			pxDst[y][x].B = Bval;
		}
	}
}



//明るさ
void imFilterContrast(ms2dImage* imgSrc, ms2dImage* imgDst, double Filval2)
{
	//Dstにコピー
	ms2dImageCopy(imgSrc, imgDst);

	//画像サイズ取得
	int wid = imgDst->width;	// 幅
	int hgt = imgDst->height;	// 高さ

	// ピクセルアクセスの準備
	ms2dColorMatrix pxSrc = imgSrc->pixels;	// 元画像のピクセルデータ
	ms2dColorMatrix pxDst = imgDst->pixels;	// 結果画像のピクセルデータ

	double lowTh = 20.0;
	double highTh = 240.0;

	// 1ピクセルずつ、すべてのピクセルを処理
	for (int y = 0; y < hgt; y++)
	{
		for (int x = 0; x < wid; x++)
		{
			// 元画像pxSrcの(x, y)座標の色値を得る
			int Rval = pxDst[y][x].R;
			int Gval = pxDst[y][x].G;
			int Bval = pxDst[y][x].B;

			//Rval
			if (Rval <= lowTh){
				Rval = 0;
			}
			else if (Rval >= highTh){
				Rval = 255;
			}
			else{
				Rval = (int)(255 / (highTh + lowTh)*(Rval + lowTh));
			}

			//Gval
			if (Gval <= lowTh){
				Gval = 0;
			}
			else if (Gval >= highTh){
				Gval = 255;
			}
			else{
				Gval = (int)(255 / (highTh + lowTh)*(Gval + lowTh));
			}

			//Bval
			if (Bval <= lowTh){
				Bval = 0;
			}
			else if (Bval >= highTh){
				Bval = 255;
			}
			else{
				Bval = (int)(255 / (highTh + lowTh)*(Bval + lowTh));
			}

			pxDst[y][x].R = (int)((double)Rval *Filval2);
			pxDst[y][x].G = (int)((double)Gval *Filval2);
			pxDst[y][x].B = (int)((double)Bval *Filval2);


		}
	}
}

//シャドウ
void imFilterContrast2(ms2dImage* imgSrc, ms2dImage* imgDst, double Filval2)
{
	//Dstにコピー
	ms2dImageCopy(imgSrc, imgDst);

	//画像サイズ取得
	int wid = imgDst->width;	// 幅
	int hgt = imgDst->height;	// 高さ

	// ピクセルアクセスの準備
	ms2dColorMatrix pxSrc = imgSrc->pixels;	// 元画像のピクセルデータ
	ms2dColorMatrix pxDst = imgDst->pixels;	// 結果画像のピクセルデータ

	double lowTh = 20.0;
	double highTh = 240.0;

	// 1ピクセルずつ、すべてのピクセルを処理
	for (int y = 0; y < hgt; y++)
	{
		for (int x = 0; x < wid; x++)
		{
			// 元画像pxSrcの(x, y)座標の色値を得る
			int Rval = pxDst[y][x].R;
			int Gval = pxDst[y][x].G;
			int Bval = pxDst[y][x].B;

			//Rval
			if (Rval <= lowTh){
				Rval = 0;
			}
			else if (Rval >= highTh){
				Rval = 255;
			}
			else{
				Rval = (int)(255 / (highTh - lowTh)*(Rval - lowTh));
			}

			//Gval
			if (Gval <= lowTh){
				Gval = 0;
			}
			else if (Gval >= highTh){
				Gval = 255;
			}
			else{
				Gval = (int)(255 / (highTh - lowTh)*(Gval - lowTh));
			}

			//Bval
			if (Bval <= lowTh){
				Bval = 0;
			}
			else if (Bval >= highTh){
				Bval = 255;
			}
			else{
				Bval = (int)(255 / (highTh - lowTh)*(Bval - lowTh));
			}

			pxDst[y][x].R = (int)((double)Rval *Filval2);
			pxDst[y][x].G = (int)((double)Gval *Filval2);
			pxDst[y][x].B = (int)((double)Bval *Filval2);


		}
	}
}
